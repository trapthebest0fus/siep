package views

import (
	"html/template"
	"io/ioutil"
	"strings"
)

// TemplateCollection regroupe tous les templates
type TemplateCollection struct {
	HomeTemplate    *template.Template
	UserNewTemplate *template.Template
}

var templates *template.Template

//PopulateTemplates charge tous les templates du répertoire templates
func PopulateTemplates() (TemplateCollection, error) {
	var templateCollection TemplateCollection
	var allFiles []string
	templatesDir := "backend/templates/"
	files, err := ioutil.ReadDir(templatesDir)
	if err != nil {
		return templateCollection, ErrorTemplateDirNotFound(templatesDir)
	}
	for _, file := range files {
		filename := file.Name()
		if strings.HasSuffix(filename, ".html") {
			allFiles = append(allFiles, templatesDir+filename)
		}
	}

	templates, err = template.ParseFiles(allFiles...)
	if err != nil {
		return templateCollection, ErrorTemplateParsing(err)
	}
	templateCollection.HomeTemplate = templates.Lookup("home.html")
	templateCollection.UserNewTemplate = templates.Lookup("userNew.html")
	// TBC ...

	return templateCollection, err

}
