package views

import "fmt"

var (
	ErrorTemplateDirNotFound = makeError("Template directory [%s] not found")
	ErrorTemplateParsing     = makeError("Template parsing error [%s]")
)

func makeError(format string) func(a ...interface{}) error {
	return func(a ...interface{}) error {
		return fmt.Errorf(format, a)
	}
}
