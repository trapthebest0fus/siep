package app

import (
	"io"
	"log"

	"github.com/spf13/viper"
)

// App object
type App struct {
	AppConfig *viper.Viper
	Trace     *log.Logger
	Info      *log.Logger
	Warning   *log.Logger
	Error     *log.Logger
}

//InitAppLogging intialize logging system
func (app *App) InitAppLogging(traceHandle, infoHandle, warningHandle, errorHandle io.Writer) {
	app.Trace = log.New(traceHandle,
		"LOG : ",
		log.Ldate|log.Ltime|log.Lshortfile)

	app.Info = log.New(infoHandle,
		"INFO: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	app.Warning = log.New(warningHandle,
		"WARN: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	app.Error = log.New(errorHandle,
		"ERR : ",
		log.Ldate|log.Ltime|log.Lshortfile)
}
