package routes

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"siep/backend/domain/accounts"
	"siep/backend/infra/globals"
	"siep/backend/infra/types"
	"siep/backend/infra/utils"
	"siep/backend/infra/views"

	"github.com/gorilla/mux"
)

func AccessControlHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}

type RouteHandler struct {
	Router             *mux.Router
	AccountService     accounts.IService
	TemplateCollection views.TemplateCollection
}

func NewAccountHandler(accountService accounts.IService, templateCollection views.TemplateCollection) *RouteHandler {
	routeHandler := &RouteHandler{
		Router:             mux.NewRouter(),
		AccountService:     accountService,
		TemplateCollection: templateCollection,
	}
	// Handler des données static
	routeHandler.Router.PathPrefix("/static/").Handler(http.FileServer(http.Dir("./frontend/public")))
	routeHandler.Router.PathPrefix("/dist/").Handler(http.FileServer(http.Dir("./frontend")))

	// Template pages
	routeHandler.Router.HandleFunc("/home/", routeHandler.HomeFunc)
	routeHandler.Router.HandleFunc("/user/new/", routeHandler.UserNew)

	// API REST
	routeHandler.Router.HandleFunc("/api/account/subscribe/", routeHandler.APIAccountSubscribe).Methods("POST")
	routeHandler.Router.HandleFunc("/api/account/login/", routeHandler.APIAccountLogin).Methods("POST")
	return routeHandler
}

// APIAccountSubscribe ...
func (h *RouteHandler) APIAccountSubscribe(w http.ResponseWriter, r *http.Request) {
	// FIXME: https://astaxie.gitbooks.io/build-web-application-with-golang/en/09.0.html
	// FIXME: qui doit faire les vérifications suivantes ? ici dans le handler de request ou à l'insertion dans la db
	// FIXME: vérifier qu'aucun des paramètres n'est vide (champs requis ou optionnel)
	// FIXME: vérifier le format attendu (un téléphone, un email, adresse existante, date)
	// FIXME: escaper l'entrée fournie par l'utilisateur (attaque xss, injection sql)
	// FIXME: bloquer un utilisateur qui serait déjà inscrit pour ne pas avoir de duplication
	type data struct {
		accounts.Account
		PasswordConfirmation string `json:"passwordConfirmation"`
	}
	var userSubscriptionData data
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		globals.App.Error.Printf("%s: ioutil.ReadAll failed\n", utils.GetCurrentFunctionName())
		http.Redirect(w, r, "/", http.StatusBadRequest)
	}
	err = json.Unmarshal(body, &userSubscriptionData)
	fmt.Printf("BODY => %s\n", string(body))
	fmt.Printf("userSubscriptionData = %#v\n", userSubscriptionData)

	// err = r.ParseForm()
	// if err != nil {
	// 	globals.App.Error.Printf("%s: ParseForm returns %v \n", utils.GetCurrentFunctionName(), err)
	// 	http.Redirect(w, r, "/", http.StatusBadRequest)
	// }
	//
	// ltBuf := r.Form.Get("inputLatitude")
	// var err1 error
	// latitude := math.NaN()
	// if ltBuf != "" {
	// 	latitude, err1 = strconv.ParseFloat(ltBuf, 32)
	// 	if err1 != nil {
	// 		globals.App.Warning.Printf("Failed to parse 'latitude' to float32 [%s]\n", ltBuf)
	// 		// TODO revoyer cette info au browser. Comment ?
	// 	}
	// }
	// var err2 error
	// lgBuf := r.Form.Get("inputLongitude")
	// longitude := math.NaN()
	// if lgBuf != "" {
	// 	longitude, err2 = strconv.ParseFloat(lgBuf, 32)
	// 	if err2 != nil {
	// 		globals.App.Warning.Printf("Failed to parse 'latitude' to float32 [%s]\n", lgBuf)
	// 		// TODO revoyer cette info au browser. Comment ?
	// 	}
	// }
	// if err1 == nil && err2 == nil {
	// 	account := &data{
	// 		Account: accounts.Account{
	// 			Email:         r.Form.Get("inputEmail"),
	// 			Password:      r.Form.Get("inputPassword"),
	// 			RaisonSociale: r.Form.Get("inputRaisonSociale"),
	// 			FirstName:     r.Form.Get("inputFirstName"),
	// 			LastName:      r.Form.Get("inputLastName"),
	// 			Phone:         r.Form.Get("inputPhone"),
	// 			Address:       r.Form.Get("inputAdresse"),
	// 			Latitude:      float32(latitude),
	// 			Longitude:     float32(longitude),
	// 		},
	// 		PasswordConfirmation: r.Form.Get("inputPasswordConfirmation"),
	// 	}
	if userSubscriptionData.Account.Password == userSubscriptionData.PasswordConfirmation {
		err = h.AccountService.Subscribe(&userSubscriptionData.Account)
		if err == nil {
			globals.App.Info.Printf("Account creation %s successfuly\n", userSubscriptionData.Account.Email)
		} else {
			globals.App.Warning.Printf("Failed to create account for %s\n", userSubscriptionData.Account.Email)
			http.Redirect(w, r, "/home/", 302)
		}
	} else {
		// TODO notifier browser que la confirmation password n'est pas bonne
	}
}

// APIAccountLogin ...
func (h *RouteHandler) APIAccountLogin(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err == nil {
		email := r.Form.Get("inputEmail")
		password := r.Form.Get("inputPassword")
		isValid := h.AccountService.Login(email, []byte(password))
		if isValid {
			globals.App.Info.Printf("Success to login for %s\n", email)
		} else {
			globals.App.Warning.Printf("Failed to login for %s\n", email)
			http.Redirect(w, r, "/home/", 302)
		}
	} else {
		globals.App.Error.Printf("%s: ParseForm returns %v \n", utils.GetCurrentFunctionName(), err)
		http.Redirect(w, r, "/", http.StatusBadRequest)
	}
}

//UserNew is used to populate the "/login/new/" URL
func (h *RouteHandler) UserNew(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		var context types.Context
		err := h.TemplateCollection.UserNewTemplate.Execute(w, context)
		if err != nil {
			log.Fatal(err)
		}
	}
}

//HomeFunc is used to populate the "/home/" URL
func (h *RouteHandler) HomeFunc(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		var context types.Context
		err := h.TemplateCollection.HomeTemplate.Execute(w, context)
		if err != nil {
			log.Fatal(err)
		}
	}
}
