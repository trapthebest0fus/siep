package server

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"siep/backend/domain/accounts"
	"siep/backend/infra/globals"
	"siep/backend/infra/server/routes"
	"siep/backend/infra/views"
	"time"

	"github.com/gorilla/handlers"
)

//getHerokuPort take the port specified by Heroku
func getHerokuPort() string {
	var port = os.Getenv("PORT")
	// Set a default port if there is nothing in the environment
	if port == "" {
		port = globals.App.AppConfig.GetString("PORT")
		globals.App.Warning.Println("No PORT environment variable detected, defaulting to " + port)
	}
	return ":" + port
}

// New returns a new HTTP server.
func New(accountService accounts.IService) *http.Server {

	templateCollection, err := views.PopulateTemplates()
	if err != nil {
		panic(err)
	}

	handler := routes.NewAccountHandler(accountService, templateCollection)

	// Middlewares
	// ... request logger
	loggingHandler := handlers.LoggingHandler(os.Stdout, handler.Router)
	accessControlHandler := routes.AccessControlHandler(loggingHandler)

	port := getHerokuPort()
	s := &http.Server{
		Addr:         port,
		Handler:      accessControlHandler,
		ErrorLog:     globals.App.Trace,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	// handling server shutdown
	go func() {
		<-quit
		globals.App.Info.Println("Server is shutting down...")

		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		s.SetKeepAlivesEnabled(false)
		if err := s.Shutdown(ctx); err != nil {
			globals.App.Error.Fatalf("Could not gracefully shutdown the server: %v\n", err)
		}
		close(done)
	}()

	// Server starting
	globals.App.Info.Printf("running server on http://localhost%s\n", port)
	globals.App.Info.Println("listening...")
	// Désactivation du https car non compatible avec heroku (TLS termination is handled by the Heroku router and can't be done at the app level)
	//log.Fatal(http.ListenAndServeTLS(port, "server.crt", "server.key", routes))
	if err := s.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		globals.App.Error.Fatalf("Could not listen on %s: %v\n", port, err)
	}
	<-done
	globals.App.Info.Println("Server stopped")

	return s
}
