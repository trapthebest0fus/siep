package types

/*
Package types is used to store the context struct which
is passed while templates are executed.
*/

//Context is the struct passed to templates
type Context struct {
}
