package utils

import (
	"fmt"
	"reflect"
	"runtime"
)

// GetFunctionName takes a function in parameter and returns the name as string
func GetFunctionName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}

// GetCurrentFunctionName returns the caller <file:line function>
func GetCurrentFunctionName() string {
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	file, line := f.FileLine(pc[0])
	return fmt.Sprintf("%s:%d %s", file, line, f.Name())
}
