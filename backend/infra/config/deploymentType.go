package config

//Enum is an interface
type Enum interface {
	Name() string
	Ordinal() int
	ValueOf() *[]string
}

//DeploymentType est le type représentant le type de déploiement de l'application
type DeploymentType int

const (
	// Unknown is the default value when deployment is invalid
	Unknown DeploymentType = iota
	// Prod for production
	Prod
	// Dev for development
	Dev
)

var deploymentConf = []string{
	"unknown",
	"prod",
	"dev",
}

// pour une génération automatique des string voir go get -u -a golang.org/x/tools/cmd/stringer

// Name returns the string
func (val DeploymentType) Name() string {
	return val.String()
}

// Ordinal returns integer value of the enum
func (val DeploymentType) Ordinal() int {
	return int(val)
}

// String returns the string
func (val DeploymentType) String() string {
	if val < Prod || val > Dev {
		return deploymentConf[0]
	}
	return deploymentConf[val]
}

// Values returns the enum array values
func (val DeploymentType) Values() *[]string {
	return &deploymentConf
}

// ToDeploymentType converts a string to DeploymentType
func ToDeploymentType(str string) DeploymentType {
	for k, v := range deploymentConf {
		if str == v {
			return DeploymentType(k)
		}
	}
	return -1 // not found
}
