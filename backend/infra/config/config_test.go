package config

import (
	"siep/backend/infra/db"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"

	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

func TestGetFileType(t *testing.T) {
	result := getFileType("config/sample.json")
	assert.Equal(t, result, "json", "Unexpected file extension")
	result = getFileType("config/sample.xml")
	assert.Equal(t, result, "xml", "Unexpected file extension")
	result = getFileType("path1/path2/path3/config/sample.xml")
	assert.Equal(t, result, "xml", "Unexpected file extension")
	result = getFileType("path1/path2/path3/config/sample.tmpl.xml")
	assert.Equal(t, result, "xml", "Unexpected file extension")
}
func TestDeploymentType(t *testing.T) {
	var deploymentConf = Prod
	assert.Equal(t, deploymentConf, Prod, "Unexpected result for prod")
	assert.Equal(t, deploymentConf.Name(), "prod", "Unexpected name for prod")
	assert.Equal(t, deploymentConf.String(), "prod", "Unexpected string for prod")
	assert.Equal(t, deploymentConf.Ordinal(), 1, "Unexpected ordinal for prod")
	deploymentConf = Dev
	assert.Equal(t, deploymentConf, Dev, "Unexpected result for dev")
	assert.Equal(t, deploymentConf.Name(), "dev", "Unexpected name for dev")
	assert.Equal(t, deploymentConf.String(), "dev", "Unexpected string for dev")
	assert.Equal(t, deploymentConf.Ordinal(), 2, "Unexpected ordinal for dev")
	deploymentConf = Unknown
	assert.NotEqual(t, deploymentConf, Dev, "Unexpected result for unknown")
	assert.NotEqual(t, deploymentConf, Prod, "Unexpected result for unknown")
	assert.Equal(t, deploymentConf.Name(), "unknown", "Unexpected name for unknown")
	assert.Equal(t, deploymentConf.String(), "unknown", "Unexpected string for unknown")
	assert.Equal(t, deploymentConf.Ordinal(), 0, "Unexpected ordinal for unknown")
}

func TestViperJsonConfig(t *testing.T) {
	// tuto rapide : http://pradippatil.com/post/viper/
	viper.SetConfigType("json")
	viper.SetConfigFile("../config/db.json")
	if err := viper.ReadInConfig(); err != nil {
		t.Errorf("Error reading config file, %s", err)
	}

	// path access
	assert.Equal(t, "ec2-54-235-90-0.compute-1.amazonaws.com", viper.Get("prod.host"), "Unexpected hostname for prod")
	assert.Equal(t, "da856qcnq4cu70", viper.Get("prod.name"), "Unexpected db name for prod")
	assert.Equal(t, "5432", viper.Get("prod.port"), "Unexpected db port for prod")
	assert.Equal(t, "yskxvptsdqvnit", viper.Get("prod.user"), "Unexpected db user for prod")
	assert.Equal(t, "edc974eec9b12b2e279a2ba1cdfabe405eac889e8b215b04dbc0504b1cca7147", viper.Get("prod.passwd"), "Unexpected db password for prod")

	// type access
	var dbCfg db.Config
	//dbCfg := new(Config)

	prod := viper.Sub("prod")
	t.Logf("prod=%#v", prod)
	err := prod.Unmarshal(&dbCfg)
	if err != nil {
		t.Errorf("unable to decode into struct, %v", err)
	}
	t.Logf("Config=%#v", dbCfg)
	assert.Equal(t, dbCfg.Host, "ec2-54-235-90-0.compute-1.amazonaws.com", "Unexpected hostname for prod")
	assert.Equal(t, dbCfg.Name, "da856qcnq4cu70", "Unexpected db name for prod")
	assert.Equal(t, dbCfg.Port, "5432", "Unexpected db port for prod")
	assert.Equal(t, dbCfg.User, "yskxvptsdqvnit", "Unexpected db user for prod")
	assert.Equal(t, dbCfg.Passwd, "edc974eec9b12b2e279a2ba1cdfabe405eac889e8b215b04dbc0504b1cca7147", "Unexpected db password for prod")
}
