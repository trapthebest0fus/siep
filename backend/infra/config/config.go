package config

import (
	"siep/backend/infra/db"
	"siep/backend/infra/globals"
	"strings"

	"github.com/spf13/viper"
)

const (
	// DbConfigFilePath is the configuration file path of the database
	DbConfigFilePath = "config/db.json"
	// AppConfigFilePath is the configuration file path of the app
	AppConfigFilePath = "config/app.json"
)

// getFileType returns the file extension without dot (filepath.Ext returns extension with dot)
func getFileType(filename string) string {
	parts := strings.Split(filename, ".")
	return parts[len(parts)-1]
}

// ReadDbConfig reads the database configuration file
func ReadDbConfig(deploymentType DeploymentType) database.Config {
	v := viper.New()
	readConfigFile(v, DbConfigFilePath)
	var dbConf database.Config
	deploymentConfig := v.Sub(deploymentType.Name())
	if deploymentConfig == nil {
		globals.App.Error.Fatalf("Invalid configuration requested")
	}
	err := deploymentConfig.Unmarshal(&dbConf)
	if err != nil {
		globals.App.Error.Fatalf("unable to decode into struct, %v", err)
	}
	return dbConf
}

// ReadAppConfig reads the app configuration file
func ReadAppConfig() *viper.Viper {
	v := viper.New()
	readConfigFile(v, AppConfigFilePath)
	return v
}

func readConfigFile(v *viper.Viper, filepath string) {
	globals.App.Info.Println("Read the configuration file :", filepath)
	v.SetConfigType(getFileType(filepath))
	v.SetConfigFile(filepath)
	if err := v.ReadInConfig(); err != nil {
		globals.App.Error.Fatalf("Error reading %s config, %s", filepath, err)
	}
}
