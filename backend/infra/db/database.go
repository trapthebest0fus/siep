package database

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

// MyDb https://stackoverflow.com/questions/28800672/how-to-add-new-methods-to-an-existing-type-in-go
// type MyDb struct {
// 	sql.DB
// }

// Store contains the db instance (postgres or sqlite3)
type Store struct {
	Db *sql.DB
}

// Config is stuct about Db information to open
type Config struct {
	Driver   string
	Host     string
	Name     string
	Port     string
	User     string
	Passwd   string
	Database string
}

func checkErr(err error) {
	if err != nil {
		fmt.Printf("PANIC ERROR %#v\n", err)
		panic(err)
	}
}

func openPostgres(dbCfg Config) *sql.DB {
	dbinfo := fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=disable", dbCfg.Host, dbCfg.Port, dbCfg.Name, dbCfg.User, dbCfg.Passwd)
	db, err := sql.Open("postgres", dbinfo)
	checkErr(err)
	return db
}

func openSqlite3(dbCfg Config) *sql.DB {
	db, err := sql.Open("sqlite3", dbCfg.Database)
	checkErr(err)
	return db
}

// Open is a dispatch wrapper to open db
func Open(dbCfg Config) (*sql.DB, error) {
	var db *sql.DB
	var err error
	switch dbCfg.Driver {
	case "postgres":
		db = openPostgres(dbCfg)
		err = nil
	case "sqlite":
		db = openSqlite3(dbCfg)
		err = nil
	default:
		err = ErrorDatabaseEngineUnsupported
	}
	return db, err
}

// IsValidUser validates email/password
func (store *Store) IsValidUser(emailToCheck string, passwdToCheck string) bool {
	isValid := false
	query := "SELECT email,password FROM users WHERE email=?"
	rows, err := store.Db.Query(query, emailToCheck)
	checkErr(err)
	var email, password string
	for rows.Next() {
		err = rows.Scan(&email, &password)
		checkErr(err)
		if password == passwdToCheck {
			isValid = true
		}
	}
	err = rows.Close()
	checkErr(err)
	return isValid
}

//UserSubscription is data type for inscription
type UserSubscription struct {
	Email             string `json:"email"`
	Password1         string `json:"password1"`
	Password2         string `json:"password2"`
	PasswordValidated string
	Telephone         string `json:"telephone"`
}

// NewUser validates email/password
func (store *Store) NewUser(user UserSubscription) bool {
	// insert into users(login,password,email,phone,created_date) values('t0f', 'XXXXXXX', 't0f@free.fr', '06.XX.XX.XX.XX', CURRENT_TIMESTAMP);
	stmt, err := store.Db.Prepare("INSERT INTO users(login, email, password, phone ,created_date) values('empty', ?,?,?,CURRENT_TIMESTAMP)")
	checkErr(err)

	_, err = stmt.Exec(user.Email, user.PasswordValidated, user.Telephone)
	checkErr(err)
	return err == nil
}
