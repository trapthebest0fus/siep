package database

import (
	"database/sql"
	"log"
	"siep/backend/domain/accounts"

	"github.com/google/uuid"
)

// Adapter
type accountRepository struct {
	db *sql.DB
}

func NewAccountRepository(db *sql.DB) accounts.IRepository {
	return &accountRepository{
		db,
	}
}

func (r *accountRepository) Subscribe(account *accounts.Account) error {
	sqlStatement := `
	INSERT INTO accounts(id, email, password, firstName, lastName, address, latitude, longitude,
						 phone, fonction, raisonSocial, plageHoraireLivraison, creationDate, isActive)
	VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)
	`
	stmt, err := r.db.Prepare(sqlStatement)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(account.ID, account.Email, account.Password,
		account.FirstName, account.LastName, account.Address,
		account.Latitude, account.Longitude, account.Phone,
		account.Fonction, account.RaisonSociale, account.PlageHoraireLivraison,
		account.CreationDate, account.IsActive)

	return err
}

func (r *accountRepository) Unsubscribe(account *accounts.Account) error {
	sqlStatement := `
	UPDATE accounts set isActive=? where id=?
	`
	stmt, err := r.db.Prepare(sqlStatement)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(account.IsActive, account.ID)

	return err
}

func (r *accountRepository) FillInformation(account *accounts.Account) error {
	sqlStatement := `
	INSERT INTO accounts(id, email, password, firstName, lastName, address, latitude, longitude,
		                 phone, fonction, raisonSocial, plageHoraireLivraison, creationDate, isActive)
	VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)
	`
	stmt, err := r.db.Prepare(sqlStatement)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(account.ID, account.Email, account.Password,
		account.FirstName, account.LastName, account.Address,
		account.Latitude, account.Longitude, account.Phone,
		account.Fonction, account.RaisonSociale, account.PlageHoraireLivraison,
		account.CreationDate, account.IsActive)

	return err
}

func (r *accountRepository) FindByID(id uuid.UUID) (*accounts.Account, error) {
	sqlStatement := `
	SELECT id, email, password, firstName, lastName, address, latitude, longitude,
		   phone, fonction, raisonSocial, plageHoraireLivraison, creationDate, isActive
	FROM accounts
	WHERE id=$1
	`
	account := new(accounts.Account)
	err := r.db.QueryRow(sqlStatement, id).Scan(&account.ID, &account.Email, &account.Password,
		&account.FirstName, &account.LastName, &account.Address,
		&account.Latitude, &account.Longitude, &account.Phone,
		&account.Fonction, &account.RaisonSociale, &account.PlageHoraireLivraison,
		&account.CreationDate, &account.IsActive)
	return account, err
}

func (r *accountRepository) FindByEmail(email string) (*accounts.Account, error) {
	sqlStatement := `
	SELECT id, email, password, firstName, lastName, address, latitude, longitude,
		   phone, fonction, raisonSocial, plageHoraireLivraison, creationDate, isActive
	FROM accounts
	WHERE email=$1
	`
	account := new(accounts.Account)
	err := r.db.QueryRow(sqlStatement, email).Scan(&account.ID, &account.Email, &account.Password,
		&account.FirstName, &account.LastName, &account.Address,
		&account.Latitude, &account.Longitude, &account.Phone,
		&account.Fonction, &account.RaisonSociale, &account.PlageHoraireLivraison,
		&account.CreationDate, &account.IsActive)
	return account, err
}

func (r *accountRepository) FindAll() (accountList []*accounts.Account, err error) {
	sqlStatement := `
	SELECT id, email, password, firstName, lastName, address, latitude, longitude,
		   phone, fonction, raisonSocial, plageHoraireLivraison, creationDate, isActive
	FROM accounts
	`
	rows, err := r.db.Query(sqlStatement)
	defer rows.Close()

	for rows.Next() {
		account := new(accounts.Account)
		if err = rows.Scan(&account.ID, &account.Email, &account.Password,
			&account.FirstName, &account.LastName, &account.Address,
			&account.Latitude, &account.Longitude, &account.Phone,
			&account.Fonction, &account.RaisonSociale, &account.PlageHoraireLivraison,
			&account.CreationDate, &account.IsActive); err != nil {
			log.Print(err)
			return nil, err
		}

		accountList = append(accountList, account)

	}
	return accountList, nil
}
