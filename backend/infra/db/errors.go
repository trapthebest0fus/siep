package database

import "errors"

var (
	ErrorDatabaseEngineUnsupported = errors.New("Database engine unsupported")
)
