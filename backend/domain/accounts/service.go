package accounts

import (
	"log"
	"time"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

// IService est un port
type IService interface {
	Subscribe(account *Account) error
	Unsubscribe(id uuid.UUID) error
	FillInformation(account *Account) error
	FindByID(id uuid.UUID) (*Account, error)
	findByEmail(email string) (*Account, error)
	FindAll() ([]*Account, error)
	Login(email string, passwd []byte) bool
}

type service struct {
	repo IRepository
}

func NewAccountService(repo IRepository) IService {
	return &service{
		repo,
	}
}

func (s *service) Subscribe(account *Account) error {
	_, err := s.repo.FindByEmail(account.Email)
	if err == nil {
		return ErrorUserAlreadySubscribed
	}
	account.ID = uuid.New()
	account.CreationDate = time.Now()
	account.IsActive = true
	account.Password, err = hashAndSalt([]byte(account.Password))
	if err != nil {
		return err
	}
	return s.repo.Subscribe(account)
}

func (s *service) Unsubscribe(id uuid.UUID) error {
	account, err := s.FindByID(id)
	if err != nil {
		return err
	}
	account.IsActive = false
	return s.repo.Unsubscribe(account)
}

func (s *service) FillInformation(account *Account) error {
	return s.repo.FillInformation(account)
}
func (s *service) FindByID(id uuid.UUID) (*Account, error) {
	return s.repo.FindByID(id)
}

func (s *service) findByEmail(email string) (*Account, error) {
	return s.repo.FindByEmail(email)
}

func (s *service) FindAll() ([]*Account, error) {
	return s.repo.FindAll()
}

func (s *service) Login(email string, passwd []byte) bool {
	account, err := s.findByEmail(email)
	if err != nil {
		return false
	}
	return comparePasswords(account.Password, passwd)
	// FIXME retourne booléen ou un CustomerID ou un RestaurantID/ProducteurId ?
}

func hashAndSalt(passwd []byte) (string, error) {
	hash, err := bcrypt.GenerateFromPassword(passwd, bcrypt.MinCost)
	return string(hash), err
}

func comparePasswords(hashedPwd string, plainPwd []byte) bool {
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}
