package accounts

import (
	"time"

	"github.com/google/uuid"
)

// Account est un compte dans le système
type Account struct {
	ID                    uuid.UUID `json:"id" db:"id"`
	Email                 string    `json:"email" db:"email"`
	Password              string    `json:"password" db:"password"`
	FirstName             string    `json:"firstName" db:"firstName"`
	LastName              string    `json:"lastName" db:"lastName"`
	Address               string    `json:"address" db:"address"`
	Latitude              float32   `json:"latitude" db:"latitude"` // DD (degrés décimaux) ou DMS (dégrés minutes secondes)
	Longitude             float32   `json:"longitude" db:"longitude"`
	Phone                 string    `json:"phone" db:"phone"`
	Fonction              string    `json:"fonction" db:"fonction"`
	RaisonSociale         string    `json:"raisonSocial" db:"raisonSocial"`
	PlageHoraireLivraison string    `json:"plageHoraireLivraison" db:"plageHoraireLivraison"`
	CreationDate          time.Time `json:"creationDate" db:"creationDate"`
	IsActive              bool      `json:"isActive" db:"isActive"`
}

// FIXME: Privé / public ?
