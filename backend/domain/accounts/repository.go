package accounts

import "github.com/google/uuid"

// IRepository est le contrat pour accèder à la base de données (dans l'archi hexa = port)
type IRepository interface {
	Subscribe(account *Account) error
	Unsubscribe(account *Account) error
	FillInformation(account *Account) error
	FindByID(id uuid.UUID) (*Account, error)
	FindByEmail(email string) (*Account, error)
	FindAll() ([]*Account, error)
}
