package accounts

import (
	"database/sql"
	"log"
	"os"
	"testing"

	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/assert"
)

// VOIR GoConvey : un outil plus poussé pour les tests avec génération de rapport structuré sur un serveur web

const database = "test.db"

var db *sql.DB

func init() {
	_, err := os.Stat(database)
	if !os.IsNotExist(err) {
		os.Remove(database)
	}
	// On initialise la db
	db = connectSqlite3(database)

}

func TestSubscribe(t *testing.T) {
	assert.NotNil(t, db, "Database is nil")

	err := createTable(db)
	assert.NoError(t, err, "createTable returns an unexpected error")

	// Crée l'implémentation de l'interface Repository
	accountRepo := NewSqlite3AccountRepository(db)
	// Instancie le domaine en injectant l'implémentation du port
	accountService := NewAccountService(accountRepo)

	// utilisation du domaine
	accounts := []*Account{
		&Account{
			Email:         "thierry-bagrand@gmail.com",
			Password:      "12345678",
			RaisonSociale: "RESTAURANT R1",
			FirstName:     "Thierry",
			LastName:      "BAGRAND",
			Phone:         "0601010101",
			Address:       "",
			Latitude:      47.25665,
			Longitude:     0.39481,
		},
		&Account{
			Email:         "bernard-rictus@gmail.com",
			Password:      "87654321",
			RaisonSociale: "RESTAURANT R2",
			FirstName:     "Bernard",
			LastName:      "RICTUS",
			Phone:         "0601010102",
			Address:       "",
			Latitude:      47.24063,
			Longitude:     0.71369,
		},
		&Account{
			Email:         "françois-leicitron@gmail.com",
			Password:      "azerty",
			RaisonSociale: "RESTAURANT R3",
			FirstName:     "François",
			LastName:      "LEICITRON",
			Phone:         "0601010103",
			Address:       "",
			Latitude:      47.18685,
			Longitude:     0.93824,
		},
		&Account{
			Email:         "jacques-lime@gmail.com",
			Password:      "ytreza",
			RaisonSociale: "RESTAURANT R4",
			FirstName:     "Jacques",
			LastName:      "LIME",
			Phone:         "0601010104",
			Address:       "",
			Latitude:      47.32107,
			Longitude:     0.98081,
		},
		&Account{
			Email:         "kelly-hole@gmail.com",
			Password:      "gfdsq",
			RaisonSociale: "RESTAURANT R5",
			FirstName:     "Kelly",
			LastName:      "HOLE",
			Phone:         "0601010105",
			Address:       "",
			Latitude:      47.34248,
			Longitude:     0.72675,
		},
		&Account{
			Email:         "chris-bishop@gmail.com",
			Password:      "qsdfg",
			RaisonSociale: "RESTAURANT R6",
			FirstName:     "Chris",
			LastName:      "BISHOP",
			Phone:         "0601010106",
			Address:       "",
			Latitude:      47.38341,
			Longitude:     0.51801,
		},
	}

	// vérif création de compte valide
	for _, account := range accounts {
		err = accountService.Subscribe(account)
		assert.NoErrorf(t, err, "Erreur lors de l'inscription du compte de %s %s", account.FirstName, account.LastName)
		assert.Equal(t, true, account.IsActive, "Compte de %s %s non activé", account.FirstName, account.LastName)
		assert.NotEmpty(t, account.CreationDate, "Absence de date de création pour le compte de %s %s non activé", account.FirstName, account.LastName)
	}

	// vérif doublon de compte
	err = accountService.Subscribe(accounts[0])
	assert.Error(t, err, "Compte en doublon de %s %s non détecté", accounts[0].FirstName, accounts[0].LastName)

	// vérif création de compte valide

	// vérif de l'api FindById()
	account, err := accountService.FindByID(accounts[0].ID)
	assert.NoError(t, err, "FindById a retourné une erreur")
	assert.Equal(t, account, accounts[0], "Le compte recherché est incohérent")

	// vérif de l'api FindAll()
	var accountList []*Account
	accountList, err = accountService.FindAll()
	assert.NoError(t, err, "FindAll a retourné une erreur")
	assert.Equal(t, len(accounts), len(accountList), "Nombre de comptes incohérents entre database et FindAll()")
	for _, account := range accountList {
		assert.NotEqual(t, 8, len(account.Password), "Password non stocké non hashé")
	}

	// vérif de l'api Unsubscribe()
	err = accountService.Unsubscribe(accounts[4].ID)
	assert.NoError(t, err, "Erreur lors de la désinscription")
	account, err = accountService.FindByID(accounts[4].ID)
	assert.Equalf(t, false, account.IsActive, "Le compte de %s %s n'est pas inactif", account.FirstName, account.LastName)

	// vérif de l'api Login()
	isLogin := accountService.Login("thierry-bagrand@gmail.com", []byte("12345678"))
	assert.Equal(t, true, isLogin, "Thierry Bagrand logged in")

	isLogin = accountService.Login("bernard-rictus@gmail.com", []byte("xxxxxxxx"))
	assert.Equal(t, false, isLogin, "Bernard Rictus login refused")
}

func createTable(db *sql.DB) error {
	// create table if not exists
	sqlStatement := `
	CREATE TABLE IF NOT EXISTS accounts(
		id TEXT primary key,
		email TEXT NOT NULL UNIQUE,
		password TEXT NOT NULL,
		firstName TEXT NOT NULL,
		lastName TEXT NOT NULL,
		address TEXT,
		latitude FLOAT,
		longitude FLOAT,
		phone TEXT NOT NULL,
		fonction TEXT,
		raisonSocial TEXT,
		plageHoraireLivraison TEXT,
		creationDate DATETIME,
		isActive INTEGER);
	`

	_, err := db.Exec(sqlStatement)
	return err
}

// côté INFRA
func connectSqlite3(database string) *sql.DB {
	db, err := sql.Open("sqlite3", database)
	if err != nil {
		return nil
	}
	return db
}

// Objet de l'infra implémentant l'interface domaine Repository (Port implémentation)
// Dans l'archi Hexa = Adapter
type accountRepository struct {
	db *sql.DB
}

func NewSqlite3AccountRepository(db *sql.DB) IRepository {
	return &accountRepository{
		db,
	}
}

func (r *accountRepository) Subscribe(account *Account) error {
	sqlStatement := `
	INSERT INTO accounts(id, email, password, firstName, lastName, address, latitude, longitude,
						 phone, fonction, raisonSocial, plageHoraireLivraison, creationDate, isActive)
	VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)
	`
	stmt, err := r.db.Prepare(sqlStatement)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(account.ID, account.Email, account.Password,
		account.FirstName, account.LastName, account.Address,
		account.Latitude, account.Longitude, account.Phone,
		account.Fonction, account.RaisonSociale, account.PlageHoraireLivraison,
		account.CreationDate, account.IsActive)

	return err
}

func (r *accountRepository) Unsubscribe(account *Account) error {
	sqlStatement := `
	UPDATE accounts set isActive=? where id=?
	`
	stmt, err := r.db.Prepare(sqlStatement)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(account.IsActive, account.ID)

	return err
}

func (r *accountRepository) FillInformation(account *Account) error {
	sqlStatement := `
	INSERT INTO accounts(id, email, password, firstName, lastName, address, latitude, longitude,
		                 phone, fonction, raisonSocial, plageHoraireLivraison, creationDate, isActive)
	VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)
	`
	stmt, err := r.db.Prepare(sqlStatement)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(account.ID, account.Email, account.Password,
		account.FirstName, account.LastName, account.Address,
		account.Latitude, account.Longitude, account.Phone,
		account.Fonction, account.RaisonSociale, account.PlageHoraireLivraison,
		account.CreationDate, account.IsActive)

	return err
}

func (r *accountRepository) FindByID(id uuid.UUID) (*Account, error) {
	sqlStatement := `
	SELECT id, email, password, firstName, lastName, address, latitude, longitude,
		   phone, fonction, raisonSocial, plageHoraireLivraison, creationDate, isActive
	FROM accounts
	WHERE id=$1
	`
	account := new(Account)
	err := r.db.QueryRow(sqlStatement, id).Scan(&account.ID, &account.Email, &account.Password,
		&account.FirstName, &account.LastName, &account.Address,
		&account.Latitude, &account.Longitude, &account.Phone,
		&account.Fonction, &account.RaisonSociale, &account.PlageHoraireLivraison,
		&account.CreationDate, &account.IsActive)
	return account, err
}

func (r *accountRepository) FindByEmail(email string) (*Account, error) {
	sqlStatement := `
	SELECT id, email, password, firstName, lastName, address, latitude, longitude,
		   phone, fonction, raisonSocial, plageHoraireLivraison, creationDate, isActive
	FROM accounts
	WHERE email=$1
	`
	account := new(Account)
	err := r.db.QueryRow(sqlStatement, email).Scan(&account.ID, &account.Email, &account.Password,
		&account.FirstName, &account.LastName, &account.Address,
		&account.Latitude, &account.Longitude, &account.Phone,
		&account.Fonction, &account.RaisonSociale, &account.PlageHoraireLivraison,
		&account.CreationDate, &account.IsActive)
	return account, err
}

func (r *accountRepository) FindAll() (accounts []*Account, err error) {
	sqlStatement := `
	SELECT id, email, password, firstName, lastName, address, latitude, longitude,
		   phone, fonction, raisonSocial, plageHoraireLivraison, creationDate, isActive
	FROM accounts
	`
	rows, err := r.db.Query(sqlStatement)
	defer rows.Close()

	for rows.Next() {
		account := new(Account)
		if err = rows.Scan(&account.ID, &account.Email, &account.Password,
			&account.FirstName, &account.LastName, &account.Address,
			&account.Latitude, &account.Longitude, &account.Phone,
			&account.Fonction, &account.RaisonSociale, &account.PlageHoraireLivraison,
			&account.CreationDate, &account.IsActive); err != nil {
			log.Print(err)
			return nil, err
		}

		accounts = append(accounts, account)

	}
	return accounts, nil
}
