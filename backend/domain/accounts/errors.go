package accounts

import (
	"errors"
)

var (
	ErrorUserAlreadySubscribed = errors.New("User already subscribed")
)
