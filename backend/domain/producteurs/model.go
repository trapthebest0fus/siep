package accounts

import "github.com/gofrs/uuid"

// Producteur est un customer dans le système
type Producteur struct {
	ID                  uuid.UUID `json:"id" db:"id"`
	AccountID           uuid.UUID `json:"accountId" db:"accountId"`
	CustomerID          uuid.UUID `json:"customerID" db:"customerID"`
	CatégorieDeProduits string    `json:"categorieDeProduit" db:"categorieDeProduit"`
}
