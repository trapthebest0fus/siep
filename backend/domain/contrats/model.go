package contrats

import (
	"time"

	"github.com/google/uuid"
)

// Contrat est ...
type Contrat struct {
	ID             uuid.UUID `json:"id" db:"id"`
	CustomerID     uuid.UUID `json:"customerId" db:"customerId"`
	WithCustomerID uuid.UUID `json:"withCustomerId" db:"withCustomerId"`
	DateSignature  time.Time `json:"dateSignature" db:"dateSignature"`
	FilePath       string    `json:"FilePath" db:"FilePath"`
}
