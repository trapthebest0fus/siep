package contrats

import (
	"siep/backend/domain/contrats"

	"github.com/google/uuid"
)

// IRepository est le contrat pour accèder à la base de données (dans l'archi hexa = port)
type IRepository interface {
	FillInformation(*Contrat) error
	FindByID(id uuid.UUID) (*Contrat, error)
	FindAll() ([]*Contrat, error)
	NewContrat(*Contrat) error
}

func (s *repository) FillInformation(*contrats.Contrat) error {
	panic("not implemented")
}

func (s *repository) FindByID(id uuid.UUID) (*contrats.Contrat, error) {
	panic("not implemented")
}

func (s *repository) FindAll() ([]*contrats.Contrat, error) {
	panic("not implemented")
}

func (s *repository) NewContrat(*contrats.Contrat) error {
	panic("not implemented")
}
