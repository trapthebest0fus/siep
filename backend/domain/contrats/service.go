package contrats

import (
	"siep/backend/domain/contrats"

	"github.com/google/uuid"
)

// IService est un port
type IService interface {
	FindByID(uuid.UUID) (*Contrat, error)

	ListerContrats(uuid.UUID) ([]Contrat, error)
	// SignerContrat signe numériquement le contrat et le stock sur le serveur
	SignerContrat(uuid.UUID) error
	TelechargerContrat(uuid.UUID) error
}

type service struct {
	repo IRepository
}

func NewContratService(repo IRepository) IService {
	return &service{
		repo,
	}
}

func (s *service) FindByID(uuid.UUID) (*contrats.Contrat, error) {
	panic("not implemented")
}

func (s *service) ListerContrats(uuid.UUID) ([]contrats.Contrat, error) {
	panic("not implemented")
}

func (s *service) SignerContrat(uuid.UUID) error {
	panic("not implemented")
}

func (s *service) TelechargerContrat(uuid.UUID) error {
	panic("not implemented")
}
