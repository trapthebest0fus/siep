package restaurants

import "github.com/google/uuid"

type EtablissementType int

const (
	NonRenseigné EtablissementType = iota
	Public
	Privé
)

var etablissementMapping = []string{
	"NonRenseigné",
	"Public",
	"Privé",
}

type EspaceDeStockageType int

const (
	Aucun EspaceDeStockageType = iota
	Faible
	Moyen
	Elevé
	Illimité
)

var espaceDeStockageMapping = []string{
	"Aucun",
	"faible",
	"moyen",
	"élevé",
	"non-limité",
}

// Restaurant est un customer dans le système
type Restaurant struct {
	ID                uuid.UUID            `json:"id" db:"id"`
	AccountID         uuid.UUID            `json:"accountId" db:"accountId"`
	CustomerID        uuid.UUID            `json:"customerID" db:"customerID"`
	TypeEtablissement EtablissementType    `json:"typeEtablissement" db:"typeEtablissement"`
	EspaceDeStockage  EspaceDeStockageType `json:"espaceDeStockage" db:"espaceDeStockage"`
}
