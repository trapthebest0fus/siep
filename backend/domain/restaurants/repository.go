package restaurants

import "github.com/google/uuid"

// IRepository est le contrat pour accèder à la base de données (dans l'archi hexa = port)
type IRepository interface {
	FillInformation(*Restaurant) error
	FindByID(id uuid.UUID) (*Restaurant, error)
}
