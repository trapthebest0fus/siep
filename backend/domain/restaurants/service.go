package restaurants

import (
	"siep/backend/domain/customers"

	"github.com/google/uuid"
)

// IService est un port
type IService interface {
	FillInformation(*customers.Customer, *Restaurant) error
	FindByID(id uuid.UUID) (*Restaurant, error)
}

type service struct {
	repo IRepository
}

func NewRestaurantService(repo IRepository) IService {
	return &service{
		repo,
	}
}

func (s *service) FillInformation(customer *customers.Customer, restaurant *Restaurant) error {
	restaurant.AccountID = customer.AccountID
	restaurant.CustomerID = customer.ID

	return s.repo.FillInformation(restaurant)
}

func (s *service) FindByID(id uuid.UUID) (*Restaurant, error) {
	return s.repo.FindByID(id)
}

// ou sont stockés les contrats (sql, nosql)
// quels sont les metadatas (date de signature)

// lister mes factures émises, passées et courantes
// lister les commandes en cours
// lire une commande terminée, en cours
