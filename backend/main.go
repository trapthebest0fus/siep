package main

import (
	"io/ioutil"
	"os"
	"siep/backend/domain/accounts"
	"siep/backend/infra/config"
	"siep/backend/infra/db"
	"siep/backend/infra/globals"
	"siep/backend/infra/server"
)

var (
	// resolve at link time
	versionApp = "unset"
	dateApp    = "unset"
	commitApp  = "unset"
	branchApp  = "unset"
)

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	globals.App.InitAppLogging(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)
	globals.App.AppConfig = config.ReadAppConfig()
	deploymentType := config.ToDeploymentType(os.Getenv("ENV"))

	globals.App.Info.Println("--------------------------")
	globals.App.Info.Println("Build information:")
	globals.App.Info.Println("--------------------------")
	globals.App.Info.Println("\t- Build date:", dateApp)
	globals.App.Info.Println("\t- Commit:", commitApp)
	globals.App.Info.Println("\t- Version:", versionApp)
	globals.App.Info.Println("\t- Branch:", branchApp)
	globals.App.Info.Println("\t- Déploiement:", deploymentType)
	globals.App.Info.Println("--------------------------")

	// Database connection & play

	if deploymentType != config.Unknown {
		dbCfg := config.ReadDbConfig(deploymentType)
		db, err := database.Open(dbCfg)
		if err != nil {
			panic(err)
		}
		defer db.Close()

		// Création l'implémentation de l'interface Repository d'un compte
		accountRepository := database.NewAccountRepository(db)
		// Instancie le domaine en injectant l'implémentation du port
		accountService := accounts.NewAccountService(accountRepository)

		server.New(accountService)
	}
}
