# Installation

## Installation de golang

Archlinux
```
sudo pacman -S go
```
Debian like
```
sudo apt-get install go ?
```
## Configuration de golang
Vérifier le PATH
```
echo $PATH
echo $GOPATH
```
Ajouter si nécessaire dans $PATH (dans ~/.bashrc) $GOPATH/bin

## Installation des packages pour une webapp

Recompilation automatique sur changement du repo (alternative CompileDaemon)
```
go get github.com/codegangsta/gin
# ou
go get https://github.com/azer/yolo
```
Gestion des dépendances

Voir installation [ici](https://golang.github.io/dep/docs/introduction.html)

dep est l'outil officiel apparamment. J'ai remplacé github.com/tools/godep par github.com/golang/dep

Heroku CLI
archlinux
```
trizen -S heroku-cli
```
Postgresql
```
go get github.com/gorilla/mux github.com/lib/pq
```
assert pour les tests
```
go get github.com/stretchr/testify/assert
```
Router gorilla mux
```
go get github.com/gorilla/mux
```
Alternative à Gorilla Mux (High performance, extensible, minimalist Go web framework)
```
go get github.com/labstack/echo
```
https://echo.labstack.com/

[jwt-go](https://github.com/dgrijalva/jwt-go) - Golang implementation of JSON Web Tokens (JWT).

Middleware gorilla
```
go get github.com/gorilla/handlers
```

## Création du repository
Mettre en place l'arborescence et les fichiers
```
git init
```
Création du Godeps.json (liste des dépendances)
```
godep save
```
Heroku détecte que c'est un déploiement d'une Webapp golang si il trouve le fichier Godeps.json

## Clone du repository
```
git clone git@bitbucket.org:trapthebest0fus/webapp-golang.git
cd webapp-golang
go install # je crois que ca va télécharger toutes les dépendances du projet
```

## Vérification
```
go build
./webapp-golang
```
On doit pouvoir se connecter à la page https://127.0.0.1:3001/

## Création du 1er commit
```
git add -A .
git commit -m "code"
```

## Création et déploiement de l'app sur Heroku

Se logger et créer l'app heroku
```
heroku login

heroku create
```
(cela crée une config git remote heroku pour le repo local)

Faire le déploiement sur le serveur heroku
```
git push heroku master

heroku open
```
## Heroku usage
```bash
heroku # affiche l'aide
heroku --version
heroku login
heroku create
heroku plugins:install someplugin
heroku update

heroku domains
heroku release
heroku status

heroku logs
heroku logs -n 200
heroku logs --tail
heroku logs --dyno router
heroku logs --source app
heroku logs --source app --dyno worker

```

# Usage
Build et run
```
go build
./webapp-golang
```
ou
Build & run en auto
```
gin -all run main.go
# ou (un web serveur permet de voir l'état de la compilation et les erreurs)
yolo -i . -e vendor -e bin -c "go run foobar.go" -a localhost:9001

```
Ouvrir dans le browser https://0.0.0.0:3001/

https://0.0.0.0:3001/api/get-name

https://0.0.0.0:3001/api/set-name?name=toto&email=toto@gmail.com

https://0.0.0.0:3001/api/get-name

## autobuild
```
gocode set autobuild true
gocode set autobuild false
```
Intéressant avec vscode pour la complétion intelligente

# GIT (en mode Github flow workflow => simple)

origin = remote Server origin
master = Master branch
feature
bugfix
hotfix
release

## Github flow

Github flow repose sur 6 points fondamentaux :

- Tout ce qui est sur master est stable et déployable
- Pour travailler sur quelque-chose, la branche doit avoir un nom significatif. Comme : feature/add-menu…
- Il faut commiter sur cette branche localement et régulièrement pusher sur une branche du même nom sur le serveur
- Une fois le développement terminé, ouvrir une pull request sur master pour recueillir du feedback et des tests.
- Une fois les feux au vert, merger sur master.
- Déployer directement après le merge

## Création d'une de feature

git pull = git fetch + git merge

git pull --rebase = git fetch + git rebase

```bash
# 1) se positionner sur la branche master pour créer une nouvelle branche
git checkout master
# 2) mise à jour de la branche master
git pull origin master
# 3) créer la nouvelle branche de feature de type 'webapp_dev_ftr-XXXX'
git branch -b webapp_dev_ftr-a_awesome_feature
# 3') alternative raccourcie, création d'une branche dérivant de origin/master (remote master)
git branch -b webapp_dev_ftr-a_awesome_feature origin/master
# 4) faire son travail de dév
git status # pour voir l'état des modifications
git add -A # indexation de tous les fichiers modifiés (vérifier les fichiers pour ne pas mettre en conf n'importe quoi)
git commit -m "message de description"
git commit # ouvre un vi qui permet de me décrire son commit
# Le mieux au moment du push est d'avoir un seul commit (historique propre à maintenir sur le master), pour ne pas créer de nouveau commit si on a besoin de continuer son travail
git commit --amend
# 5) Faire un rebase de sa branche
git pull --rebase origin master
# 7) Push de sa branche (-u car la branche, si elle n'a pas été créée sur le serveur, n'est pas connue)
git push -u origin webapp_dev_ftr-a_awesome_feature
# 8) Créer la pull request en suivant le lien issu du git push précédent
# 9) Attendre les revues des autres dev
# 10) prendre en compte les remarques (git commit --amend)
# 11) refaire un push (pas besoin de nouvelle pull request)
# 12) refaire les étapes 9) 10) 11) jusqu'à que tout le monde est validé et que l'intégrateur est fusionné
```
## Git squash (modification de l'historique)
L'objectif est de travailler son historique pour le rendre propre pour la fusion dans le cas où on n'a pas fait de git commit --ammend

## Git stash (remise un travail à plus tard)
Fait une photo de l'état courant (fichiers indexés et non indexés) pour les restaurer plus tard sur une branche


## autres
git reflog



# Les tests

## Créer un test

un test est un fichier go portant le même nom que la pièce testée avec le suffix "_test". Voir api_test.go.

Ce fichier peut contenir un ou des tests.
func TestXxx(*testing.T)
func BenchmarkXxx(*testing.B)

[Testing](https://golang.org/pkg/testing/)


## Run d'un ou des test(s)

Lancer les tests locaux au répertoire
```
go test
```
Avoir plus d'information
```
go test -v
```
Lancer un test spécifique localement
```
go test . -v -run TestAPIGetNameFunc
```

Lancer tous les tests récursivement sur l'arbo
```
go test ./...
```

# Sécurité

## HTTPS / TLS

### source

https://www.kaihag.com/https-and-go/

explique la gestion des clés self-signed en automatique pour le dev
explique comment faire une clé en PROD
redirigé le http vers le https


### Generate private key (.key)
#### Key considerations for algorithm "RSA" ≥ 2048-bit
```
openssl genrsa -out server.key 2048
```
Key considerations for algorithm "ECDSA" ≥ secp384r1
List ECDSA the supported curves (openssl ecparam -list_curves)
```
openssl ecparam -genkey -name secp384r1 -out server.key
```
#### Generation of self-signed(x509) public key (PEM-encodings .pem|.crt) based on the private (.key)
```
openssl req -new -x509 -sha256 -key server.key -out server.crt -days 365
```
#### En une seule commande
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout server.key -out server.crt
```
challenge trapthebest0fus

# Ressources

[webapp-with-golang-anti-textbook](https://thewhitetulip.gitbooks.io/webapp-with-golang-anti-textbook/content/)

[webapp-with-golang-anti-textbook : Repository](https://github.com/thewhitetulip/Tasks)

[Building a Go Web App](https://grisha.org/blog/2017/04/27/simplistic-go-web-app/)

[wiki golang : writing web Applications](https://golang.org/doc/articles/wiki/)

[Go By Example](https://gobyexample.com/)

[Golang Awesome](https://github.com/avelino/awesome-go)

[Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

[Building Web Apps with Go](http://codegangsta.gitbooks.io/building-web-apps-with-go/content/)

[Build Web Application with Golang](http://astaxie.gitbooks.io/build-web-application-with-golang/content/en/index.html)

[Des livres sur Go](https://github.com/dariubs/GoBooks)

[Comparaison de framework web pour Go](https://nordicapis.com/7-frameworks-to-build-a-rest-api-in-go/)

[présentation des go templates](https://gohugo.io/templates/introduction/)

[Des exemples complets de sites web](https://www.reddit.com/r/golang/comments/1vlj9e/complete_opensource_go_web_apps/)

# Sources

https://leanpub.com/howtodeployagowebapptoheroku101/read


## A approfondir

app.go
models.go

middleware

router

validation

sessions

postgres / sqlite3 pour le local

cookie

golang + react (script es6 jsx)

redux (state management)

flux de facebook ?

redis

push notification server

webnotification

Graphql vs API REST (optimisé pour les requetes complexes)

logging & tracing http request https://gist.github.com/enricofoltran/10b4a980cd07cb02836f70a4ab3e72d7

# TODO
logguer les requetes et les réponses du serveur de façon globale

# Resilience (voir chaos engineering sur conf Devoxx fr)

Pattern de contre mesure :

* timeout
* retry
* circuit breaker
* communication _asynchrone_ (éviter le multithread à cause de la monter en charge+latence et les appels bloquants)
* health check
* location transparency (load balancing ?)
* service discovery
* message queues (temporal decoupling)

Techniques d'attaques :

* injection de clients
* tuer des parties du système (backend, db, ...)
* injection de failure (pertes de paquets, ...)
* augmenter les latences de réponses (faible/élevée, linéaires, aléatoires)
* injecter des bad requests (par ex. des forms avec plus moins de paramètres que ce qui étaient attendus, valeurs démentes, image de 1Go, 2Go...)
* injecter des large requests
* partial kill
* rolling update



Outils :

* pumpa
* gatlin / vrk (injection de clients)