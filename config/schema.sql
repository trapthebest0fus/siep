CREATE TABLE IF NOT EXISTS accounts(
    id TEXT primary key,
    email TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL,
    firstName TEXT NOT NULL,
    lastName TEXT NOT NULL,
    address TEXT,
    latitude FLOAT,
    longitude FLOAT,
    phone TEXT NOT NULL,
    fonction TEXT,
    raisonSocial TEXT,
    plageHoraireLivraison TEXT,
    creationDate DATETIME,
    isActive INTEGER);

