SHELL:=/bin/bash

DATABASE=webapp.db

# mode -silent par défaut pour ne pas voir les messages qui traces les changements
# de répertoire des appels $(MAKE) dasn les sous-répertoires
MAKEFLAGS += --silent

.PHONY: all
all: help

.PHONY: help
help: makefile
	@echo "Choose a command run in "$(PROJECTNAME)":"
	@sed -n 's/^##//p' $< | column -t -s '|' |  sed -e 's/^/ /'

.PHONY: clean
## clean | clean backend
clean:
	@(cd backend && $(MAKE) clean)

.PHONY: clean-all
## clean-all | clean backend, frontend and node_modules
clean-all: clean
	@(cd frontend && $(MAKE) clean)

.PHONY: build
## build | Build frontend and banckend
build:
	@(cd backend  && $(MAKE) build)
	@(cd frontend && $(MAKE) build)

.PHONY: run
## run | Run the webapp
run: build
	@(cd backend  && $(MAKE) run)

.PHONY: heroku
## heroku | Run the webapp for heroku
heroku: build
	@(cd backend  && $(MAKE) heroku)


.PHONY: deploy
## deploy | Deploy master branch to heroku (need to be on clean master branch)

# deploy: build
# 	@echo "Deploy to heroku..."
# 	@if [ $(shell git rev-parse --abbrev-ref HEAD) = "master" ]; then \
# 		if  [ -z "$(shell git status --porcelain)" ]; then \
# 			git push heroku master; \
# 		else \
# 			echo "Master branch is no clean (see git status)"; \
# 		fi; \
# 	else \
# 	  	echo "Deploy only master branch"; \
# 	fi

deploy: build
	@echo "Deploy to heroku..."
	@if  [ -z "$(shell git status --porcelain)" ]; then \
		git push heroku $(shell git rev-parse --abbrev-ref HEAD):master -f; \
	else \
		echo "Master branch is no clean (see git status)"; \
	fi; \

.PHONY: test
## test | Test backend & frontend
test:
	@(cd backend  && $(MAKE) test)
	@(cd frontend && $(MAKE) test)

.PHONY: init-db
## init-db | Initialize sqlite3 database for dev mode
init-db: config/schema.sql
	@cat $^ | sqlite3 $(DATABASE)
	@echo "$(DATABASE) created"


.PHONY: lint
## lint | Check coding rules on backend & frontend
lint:
	@echo "Check coding rules, reports suspicious constructs..."
	@(cd backend  && $(MAKE) lint)
	@(cd frontend && $(MAKE) lint)


.PHONY: deps
## deps | Install go dependencies (tools & libs)
deps:
	# tools for go
	go get github.com/alecthomas/gometalinter
	gometalinter --install
	go get github.com/zmb3/gogetdoc
	go get github.com/sqs/goreturns
	go get github.com/nsf/gocode
	go get golang.org/x/tools/cmd/gorename
	go get golang.org/x/tools/cmd/goimports
	go get golang.org/x/lint/golint
	go get github.com/codegangsta/gin
	go get github.com/azer/yolo
	# project dependencies
	dep ensure
	dep status

config/server.key config/server.crt:
	@echo "Generate keys for https..."
	@pushd config
	openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout config/server.key -out config/server.crt
	@popd

.PHONY: gen_keys
## gen_keys | Generate keys for https webserver
gen_keys: config/server.key config/server.crt

